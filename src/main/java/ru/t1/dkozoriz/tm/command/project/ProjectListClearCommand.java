package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.Nullable;

public final class ProjectListClearCommand extends AbstractProjectCommand {

    public ProjectListClearCommand() {
        super("project-clear", "delete all projects.");
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        @Nullable final String userId = getUserId();
        getProjectService().clear(userId);
    }

}
