package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListShowCommand extends AbstractProjectCommand {

    public ProjectListShowCommand() {
        super("project-list", "show project list.");
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECTS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @Nullable final String userId = getUserId();
        @Nullable final List<Project> projects = getProjectService().findAll(userId, sort);
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project.getName());
            index++;
        }
    }

}