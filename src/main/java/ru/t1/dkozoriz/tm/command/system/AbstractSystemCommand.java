package ru.t1.dkozoriz.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.command.AbstractCommand;
import ru.t1.dkozoriz.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    public AbstractSystemCommand(@NotNull String name, @Nullable String description) {
        super(name, description);
    }

    public AbstractSystemCommand(@NotNull String name, @Nullable String description, @Nullable String argument) {
        super(name, description, argument);
    }

    public Role[] getRoles() {
        return null;
    }

}