package ru.t1.dkozoriz.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public ApplicationAboutCommand() {
        super("about", "show developer info.", "-a");
    }

    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Daria Kozoriz");
        System.out.println("E-mail: dkozoriz@t1-consulting.ru");
    }

}
