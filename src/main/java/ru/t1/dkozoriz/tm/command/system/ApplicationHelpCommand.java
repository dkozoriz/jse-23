package ru.t1.dkozoriz.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.command.AbstractCommand;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    public ApplicationHelpCommand() {
        super("help", "show command list.", "-h");
    }

    public void execute() {
        for (@NotNull final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            System.out.println(command);
        }
    }

}
